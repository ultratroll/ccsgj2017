﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cthulhu
{
    public int score;
    public float xposition;
   

    public Cthulhu()
    {
        xposition = 4f;
        score = 0;
    }
}

public class SeaController : MonoBehaviour
{
    // Default sea behavior
    public float seaSpeed = 0.1f;
    public float seaHeight = 1f;
    private float sinPos = 0f;
    // 

    public bool isSplashing = false;
    
    public MeshFilter meshFilter;
    public Mesh mesh;
    //public Cthulhu player;
    private Vector3[] vertices;
    public Chuthulu chuthulu;

    public Transform player;

    // Wave managment
    public float wavePosition;  // Position where wave ocurrs, the epicenter
    public float waveTime;        // Time duration of the wave
    public float waveRadius = 1f;
    public float waveIntensity = 3f;

    private float SplashRange=2f;
    private float SplashIntensity = 2f;

    public GameObject vfxSplash;

    // Use this for initialization
    void Start()
    {
        mesh = meshFilter.mesh;
        vertices = mesh.vertices;
        //player = new Cthulhu();
        wavePosition = 0f ;
        Debug.Log("vertices: " + vertices.Length);
        //for (int i = 0; i < vertices.Length; i++) {
        //    Debug.Log("- "+i+": "+vertices[i]);
        //}
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        InputAction();
        UpdateMesh();
    }

    void UpdateMesh()
    {
        for (var i = 0; i < mesh.vertexCount; i++)
        {
            var vert = vertices[i];
            vert.Set(vert.x, SeaSin(vert.x) + SplashWave(vert.x), vert.z);
            vertices[i] = vert;
            sinPos += seaSpeed;
        }

        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        if (isSplashing) {
            waveTime -= Time.deltaTime;
            if (waveTime < 0) {
                isSplashing = false;
            }
        }
        transform.GetComponent<MeshCollider>().sharedMesh = mesh;

    }


    float SplashWave(float vertX)
    {
        var result = 0f;
        if (isSplashing && waveRadius > Mathf.Abs(vertX - wavePosition))
        {
            float firstx = wavePosition-waveRadius;
            float xParametrico = (vertX-firstx)/(2*waveRadius);

            result = waveIntensity * Mathf.Sin(-waveTime * Mathf.PI) * Mathf.Sin(xParametrico * Mathf.PI);

            //if (vfxSplash != null) vfxSplash.SetActive(true);
            //Invoke("HideSplashVFX",2.0f);
        }
        return result;
    }

    void CreateSplashVFX()
    {
        GameObject spawn= (GameObject)Instantiate(vfxSplash, player.position+ new Vector3(0,10f,0), Quaternion.identity);
        chuthulu.PleaseWait(1f);
    }

    /*
    void HideSplashVFX()
    {
        vfxSplash.SetActive(false);
    }
    */

    float SeaSin(float vert)
    {
        return seaHeight * Mathf.Sin(vert + sinPos);
    }

    float Splash(float vert)
    {
        return seaHeight * Mathf.Sin(vert + sinPos);
    }

    void DoSplash(float xposition) {
        isSplashing = true;
        wavePosition = xposition;
        waveTime = 2f;
        CreateSplashVFX();
    }

    void InputAction()
    {
        if (Input.GetKeyDown("space") && !isSplashing )
        {
            DoSplash(player.position.x/10);
        }
    }

    void ApplySplash()
    {
        ;
    }

    /*
    // deprecate
    IEnumerator BeginSplash(float waitTime, int VertZ= 60)
    { 
        float t = 0.0f;
        bIsSplashing = true;
        //yield return new WaitForSeconds(waitTime);
        //print("WaitAndPrint " + Time.time);


        //splashes[VertZ] = SplashIntensity;

        while (t <= 1)
        {
            //splashes[VertZ] = Mathf.Lerp(0, SplashIntensity, t);
            t += 0.05f * Time.deltaTime;
            //Debug.Log("up"+splashes[VertZ]);       
        }

        t = 0.0f;
        while (t <= 1)
        {
            //splashes[VertZ] = Mathf.Lerp(SplashIntensity, 0, t);
            t += 0.05f * Time.deltaTime;
            //Debug.Log("down"+splashes[VertZ]);
        }

        bIsSplashing = false;

        yield return null;
    }
    */
}
