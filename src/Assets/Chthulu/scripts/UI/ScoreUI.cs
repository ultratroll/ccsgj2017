﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour {

    Text text;

    // Use this for initialization
    void Start () {
        text= this.GetComponent<Text>();
        text.text = ""+GameManager.Score;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        text.text = "" + GameManager.Score;
    }
}
