﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResetUI : MonoBehaviour {

    Image bar;
    public Text rageLabel;
    bool bGettingGameOver;

    // Use this for initialization
    void Start () {
        GameManager.Anger = 0;
        GameManager.Score = 0;
    }
	
}
