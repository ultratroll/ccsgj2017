﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RageUI : MonoBehaviour {

    Image bar;
    public Text rageLabel;
    bool bGettingGameOver;

    // Use this for initialization
    void Start () {
        bar= this.GetComponent<Image>();
        bar.fillAmount = GameManager.Anger / 3;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        bar.fillAmount= (float)GameManager.Anger / 3f;
        UpdateLabel();
    }

    void UpdateLabel()
    {
        if (GameManager.Anger == 0) rageLabel.text = "Chuthulu mood: CHUTHULU IS TRYING TO SLEEP";
        if (GameManager.Anger == 1) rageLabel.text = "Chuthulu mood: CHUTHULU IS PISSED OFF, STUPID SHIPS";
        if (GameManager.Anger == 2) rageLabel.text = "Chuthulu mood: CHUTHULU IS GETTING REALLY ANGRY";
        if (GameManager.Anger>=3 && !bGettingGameOver)
        {
            rageLabel.text = "Chuthulu mood: CHUTHULU IS IN FULL RAGE, BA BAY";
            bGettingGameOver = true;
            Invoke("GameOver", 4f);
            
        }
    }

    void GameOver()
    {
        Application.LoadLevel("GameOver");
    }
}
