﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class ShipController : MonoBehaviour {
	public Vector3 speed;
	public float health;
	public bool dead;
	public float trans;
	private static float MAX_HEALTH = 60f;
	private static float FLIP_ROTATION = 0.8f;
	public float rotation;

	public CapsuleCollider colliderCapsule;
	
	// Use this for initialization
	void Start () {
		//speed = new Vector3(1f,0f);
		health = MAX_HEALTH;
		dead = false;

	}

	bool CheckFlip (){
		rotation = colliderCapsule.transform.rotation.z;
		bool result = rotation>FLIP_ROTATION || rotation < -FLIP_ROTATION;
		return result;
	}

	bool CheckCollision() {
		bool result = false;
		return result;
	}

	void SetTransparency(float trans){
		this.trans = trans;
	}

	void SinkMe () {
		if (health > 0) {
			if (health < MAX_HEALTH - 1) {
				colliderCapsule.enabled = true;
			}
			health -= Time.deltaTime;
			SetTransparency (health / MAX_HEALTH);
		} else {
			Destroy(gameObject);
		}
	}

	void CrashMe ()
	{
	}

	public void TurnBack(){

		this.speed = new Vector3(-speed.x, speed.y, speed.z);

		colliderCapsule.transform.localScale = new Vector3(-colliderCapsule.transform.localScale.x, colliderCapsule.transform.localScale.y, colliderCapsule.transform.localScale.z);
	}

	// Update is called once per frame
	void Update () {
		if (colliderCapsule.transform.position.y > -2 && CheckFlip()) {
			colliderCapsule.enabled = false;
			health = MAX_HEALTH;
			if (!dead) {
				GameManager.Score++;
			}
			dead = true;

			SinkMe ();
		}
		if (colliderCapsule.transform.position.y < -40) {
			if (!dead) {
				GameManager.Anger++;
				Debug.Log ("Anger:" + GameManager.Anger);
			}
			Destroy (gameObject);
		} else if (!dead) {
			transform.Translate (
				speed.x * Time.deltaTime,
				speed.y * Time.deltaTime,
				speed.z * Time.deltaTime
			);
		} else {
			SinkMe ();

		}

	}
}
