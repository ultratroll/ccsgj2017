﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipGenerator : MonoBehaviour {

    public GameObject ship_basic;
    public GameObject ship_2;
    public GameObject ship_3;

    public float ship_basic_frequency;
    public float ship_2_frequency;
    public float ship_3_frequency;

    public float TimeFirstSpawn  = 2.0f;
    public float TimeRepeatSpawn = 3.0f;

    public Transform SpawnLeft_1;
    public Transform SpawnLeft_2;
    public Transform SpawnLeft_3;

    public Transform SpawnRight_1;
    public Transform SpawnRight_2;
    public Transform SpawnRight_3;

    // Use this for initialization
    void Start () {
        InvokeRepeating("SpawnRamdomShip", TimeFirstSpawn, TimeRepeatSpawn);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SpawnRamdomShip()
    {
        float randomBoat= Random.Range(0f, 1f);

        if (randomBoat < ship_2_frequency)      SpawnShip(ship_2);

        randomBoat = Random.Range(0f, 1f);

        if (randomBoat < ship_3_frequency)      SpawnShip(ship_3);
        else                                    SpawnShip(ship_basic);
    }

    void SpawnShip(GameObject shipToSpawn)
    {
        GameObject ship = null;

        bool left = Random.Range(0, 2)>0;
        int index = Random.Range(0, 3);

        if (left)
        {
            if (index == 0) ship = (GameObject)Instantiate(shipToSpawn, SpawnLeft_1.position, SpawnLeft_1.rotation);
            if (index == 1) ship = (GameObject)Instantiate(shipToSpawn, SpawnLeft_2.position, SpawnLeft_2.rotation);
            if (index == 2) ship = (GameObject)Instantiate(shipToSpawn, SpawnLeft_3.position, SpawnLeft_3.rotation);

            
        }
        else
        {
            if (index == 0) ship = (GameObject)Instantiate(shipToSpawn, SpawnRight_1.position, SpawnRight_1.rotation);
            if (index == 1) ship = (GameObject)Instantiate(shipToSpawn, SpawnRight_2.position, SpawnRight_2.rotation);
            if (index == 2) ship = (GameObject)Instantiate(shipToSpawn, SpawnRight_3.position, SpawnRight_3.rotation);

            ShipController controller= ship.GetComponent<ShipController>();
            controller.TurnBack();
        }
    }
}
