﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chuthulu : MonoBehaviour {

    public float speed;
    public float waitTime = 0;
    public Animator animator;
    public bool lookingLeft=true;
    public GameObject front;
    public GameObject left;
    public GameObject right;

    // Use this for initialization
    void Start () {

    }

    public void PleaseWait(float t) {
        this.waitTime = t;
    }
	// Update is called once per frame
	void FixedUpdate () {
        if (waitTime > 0) {
            waitTime -= Time.deltaTime;
            front.SetActive(true);
            left.SetActive(false);
            right.SetActive(false);
        }
        else
        {
            front.SetActive(false);
            if (lookingLeft)
            {
                left.SetActive(true);
                right.SetActive(false);
            }
            else {
                left.SetActive(false);
                right.SetActive(true);
            }

            if (this.transform.position.x > 31)
            {
                speed *= -1;
                lookingLeft = true;
    
            }

            if (this.transform.position.x < -31)
            {
                speed *= -1;
                lookingLeft = false;
            }

            this.transform.position += new Vector3(speed, 0, 0);
        }
	}
}
