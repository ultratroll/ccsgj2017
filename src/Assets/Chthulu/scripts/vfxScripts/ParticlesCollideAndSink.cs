﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlesCollideAndSink : MonoBehaviour {

    public ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    
    // Use this for initialization
    void Start () {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }

    void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        var col = part.collision;
        col.enabled = false;
    }
}
