﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepOrientation : MonoBehaviour {
    Transform myParent;
	// Use this for initialization
	void Start () {
        myParent = transform.parent;
	}
	
	// Update is called once per frame
	void Update () {
        transform.localRotation = Quaternion.Euler(0, 0, myParent.localRotation.eulerAngles.z * -1);
	}
}
