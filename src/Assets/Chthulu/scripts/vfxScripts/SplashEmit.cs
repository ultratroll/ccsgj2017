﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashEmit : MonoBehaviour {
    [SerializeField]
    ParticleSystem partSystem;
    [SerializeField]
    int emitNumber = 40;

    [SerializeField]
    bool useRandomEmit;
    [SerializeField]
    int randMin = 1, randMax = 5;

    float cooldown = 1.0f;
    float timePartEmited = 0.0f;
    bool emited = false;
    void Update()
    {
        if (emited)
        {
            if ((Time.time - timePartEmited) >= cooldown)
            {
                emited = false;
            }
        }
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "SeaWater")
        {
            if (!emited)
            {
                emited = true;
                timePartEmited = Time.time;
                //partSystem.gameObject.SetActive(true);
                if (useRandomEmit)
                {
                    emitNumber = Random.Range(randMin, randMax);
                    Debug.Log(emitNumber);
                }
                partSystem.Emit(emitNumber);
            }
        }
    }
}
