﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class level : MonoBehaviour {

	public float movementSpeed = 0;

	void Update(){

		transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
		movementSpeed += -0.0001f;
	}

}
