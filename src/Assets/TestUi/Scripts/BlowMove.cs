﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowMove : MonoBehaviour {
	public float moveSpeed;
	public ParticleSystem vfxBall;
	//public ModifyMesh sea;

	GameObject MoveThisObjectWithSound;//public GameObject MoveThisObjectWithSound;
	AudioSource audio;

	public StartAction startAction;

	void Start () {
		//moveSpeed=1f;

		MoveThisObjectWithSound = this.gameObject; //MoveThisObjectWithSound = GameObject.FindGameObjectWithTag("Cube");
		audio = gameObject.AddComponent<AudioSource>() as AudioSource;
		audio.clip = Microphone.Start("Built-in Microphone", true, 1, 44100);

	}


	void Update () {
		transform.Translate(moveSpeed*Input.GetAxis("Horizontal")*Time.deltaTime,0f,moveSpeed*Input.GetAxis("Vertical")*Time.deltaTime);


		if(audio.isPlaying){}else if(audio.clip.isReadyToPlay){audio.Play();}else{audio.clip = Microphone.Start("Built-in Microphone", true,1, 44100);}
		float y = audio.GetSpectrumData(512,0,FFTWindow.Hanning)[64] * 1000000;


		if(y >= 12000){
			vfxBall.Emit (40);
			startAction.DoStart ();
		}
			
		if(y >= 500){
			GetComponent<Rigidbody>().AddForce (0,1,0);
			vfxBall.Emit (2);
		}

		if(y >= 120){
			GetComponent<Rigidbody>().AddForce (0,1,0);
			vfxBall.Emit (1);
		}

		if(y >= 70){
			GetComponent<Rigidbody>().AddForce (0,1,0);
		}

		if(y <= -10){
			GetComponent<Rigidbody>().AddForce (0,-0.5F,0);
		}

	}

}﻿
