﻿using UnityEngine;
using System.Collections;
//Add this script to an Object to move it with microphone sound. Sound is translated to a float y and then Vector3.y
public class AudioTest : MonoBehaviour {

    public ModifyMesh sea;

    GameObject MoveThisObjectWithSound;//public GameObject MoveThisObjectWithSound;
    AudioSource audio;
    void Start () 
    {
        MoveThisObjectWithSound = this.gameObject; //MoveThisObjectWithSound = GameObject.FindGameObjectWithTag("Cube");
        audio = gameObject.AddComponent<AudioSource>() as AudioSource;
        audio.clip = Microphone.Start("Built-in Microphone", true, 1, 44100);
    }
    
    void Update () 
    {

        if(audio.isPlaying){}else if(audio.clip.isReadyToPlay){audio.Play();}else{audio.clip = Microphone.Start("Built-in Microphone", true,1, 44100);}
        float y = audio.GetSpectrumData(128,0,FFTWindow.BlackmanHarris)[64] * 1000000;
    


        if(y >= 15){
			sea.seaHeight = Mathf.Lerp(sea.seaHeight, Mathf.Clamp(y/200,0.1f,1.0f), Time.deltaTime *12.0f);
            //MoveThisObjectWithSound.transform.position += new Vector3(1,0,0) * 5 * Time.deltaTime;
        }


    }

}﻿