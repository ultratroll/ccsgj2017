﻿using UnityEngine;
using System.Collections;

public class MoveUpDown : MonoBehaviour {

	public Transform farEnd;
	private Vector3 frometh;
	private Vector3 untoeth;
	public float secondsForOneLength = 6f;

	void Start()
	{
		frometh = transform.localPosition;
		untoeth = farEnd.localPosition;
	}

	void Update()
	{
		transform.localPosition = Vector3.Lerp(frometh, untoeth, Mathf.SmoothStep(0f,1f, Mathf.PingPong(Time.time/secondsForOneLength, 1f)));
	}
}