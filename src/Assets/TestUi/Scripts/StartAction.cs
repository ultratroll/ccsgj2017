﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAction : MonoBehaviour {

	public Rigidbody rigidBodyReference;
	public level levelReference;

	public void DoStart () {
		rigidBodyReference.isKinematic = false;
		levelReference.enabled = true;
		rigidBodyReference.useGravity = true;
		this.enabled = false;
	}
}
