﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddWaveForce : MonoBehaviour {

    [SerializeField]
    float radius = 5.0f;
    [SerializeField]
    float wavePower = 10.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1"))
        {
            Vector3 waveSourcePos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(waveSourcePos, radius);
            foreach (Collider col in colliders)
            {
                Rigidbody rb = col.GetComponent<Rigidbody>();
                if (rb != null)
                    rb.AddExplosionForce(wavePower, waveSourcePos, radius, 3.0f);
            }
        }
	}
}
