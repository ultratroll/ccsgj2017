﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2DControl : MonoBehaviour {
    [SerializeField]
    float playerSpeed;

    [SerializeField]
    GameObject Sonar;
    [SerializeField]
    float sonarSpeed = 1.0f;
    [SerializeField]
    float sonarCooldown = 2.0f;
    [SerializeField]
    float sonarLength = 10.0f;
    bool useSonar;
    float timeStartLerp;
    ParticleSystem sonarPartSyst;
    //CircleCollider2D sonarTrigger;

    Rigidbody2D myRigidBody;
    bool facingRight;

    bool jump;
    bool isGrounded;
    [SerializeField]
    Transform groundCheck;
    float groundRadius = 0.2f;
    [SerializeField]
    LayerMask whatIsGround;
    [SerializeField]
    float jumpForce = 70;

    SpriteRenderer mySpriteRend;
	// Use this for initialization
	void Start () {
        myRigidBody = GetComponent<Rigidbody2D>();
        mySpriteRend = GetComponent<SpriteRenderer>();
        facingRight = true;
        isGrounded = false;
        sonarPartSyst = Sonar.GetComponent<ParticleSystem>();
        //sonarTrigger = Sonar.GetComponent<CircleCollider2D>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
        /*
        if (isGrounded && myRigidBody.gravityScale != 0)
            myRigidBody.gravityScale = 0;
        else
            if (!isGrounded && myRigidBody.gravityScale == 0)
                myRigidBody.gravityScale = 1.0f;
        */    
        //Set Animator Grounded Boolean value here
        //Set Animator Speed V Float Value

        float move = Input.GetAxis("Horizontal");
        myRigidBody.velocity = new Vector2(move * playerSpeed, myRigidBody.velocity.y);
        //Set Animator SpeedH Float Value here.
        //

        if (move < 0 && facingRight)
        {
            facingRight = false;
            mySpriteRend.flipX = true;
        }
        else
            if (move > 0 && !facingRight)
            {
                facingRight = true;
                mySpriteRend.flipX = false;
            }
	}

    void Update() {
        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            //Set Grounded False in the animator
            myRigidBody.AddForce(new Vector2(0, jumpForce));
        }
        if (Input.GetButtonDown("Fire1"))
        {
            timeStartLerp = Time.time;
            useSonar = true;
            sonarPartSyst.Emit(1);
        }

        if (useSonar)
        {
            if ((Time.time - timeStartLerp) / sonarSpeed < 0.98f)
            {
                Sonar.transform.localScale = Vector3.Lerp(new Vector3(0.1f, 0.1f, 0.1f), new Vector3 (sonarLength, sonarLength, 1), (Time.time - timeStartLerp) / sonarSpeed);
            }
            else
            {
                useSonar = false;
                Sonar.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            }
        }
    }
}
