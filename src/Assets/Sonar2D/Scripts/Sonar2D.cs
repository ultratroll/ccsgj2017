﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonar2D : MonoBehaviour {
    [SerializeField]
    float sonarSpeed = 1.0f;
    [SerializeField]
    float sonarCooldown = 2.0f;
    [SerializeField]
    float sonarLength = 10.0f;

    CircleCollider2D sonarTrigger;
    float timeStartLerp;
    bool useSonar;
    // Use this for initialization
	void Start () {
        sonarTrigger = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1"))
        {
            timeStartLerp = Time.time;
            useSonar = true;
        }
        
        if (useSonar)
        {
            if (sonarTrigger.radius < sonarLength)
            {
                sonarTrigger.radius = Mathf.Lerp(0.1f, sonarLength, (Time.time - timeStartLerp) / sonarSpeed);
            }
            else
            {
                useSonar = false;
                sonarTrigger.radius = 0.1f;
            }
        }
	}
}
