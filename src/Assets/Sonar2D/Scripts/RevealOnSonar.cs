﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RevealOnSonar : MonoBehaviour {
    float revealTime = 5.0f;
    float timeWhenRevealed;
    bool hide;
    SpriteRenderer mySpriteRend;
	// Use this for initialization
	void Start () {
        mySpriteRend = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (hide)
        {
            mySpriteRend.color = Color.Lerp(Color.white, Color.black, (Time.time - timeWhenRevealed) / revealTime);
            if (mySpriteRend.color == Color.black)
                hide = false;
        }
	}

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "SonarPlayer")
        {
            Debug.Log("test");
            mySpriteRend.color = Color.white;
            timeWhenRevealed = Time.time;
            hide = true;
        }   
    }

}
