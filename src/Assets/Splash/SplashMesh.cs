﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashMesh : MonoBehaviour {

    public MeshFilter meshFilter;
    public Mesh mesh;
    private Vector3[] vertices;

    public float seaSpeed = 0.1f;
    public float seaHeight = 1f;

    public float sinPos = 0f;
    // Use this for initialization
    void Start () {
        mesh = meshFilter.mesh;
        vertices = mesh.vertices;
        Debug.Log("vertices: "+vertices.Length);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        UpdateMesh();
    }
	float basicWave(float x) {
		return seaHeight * Mathf.Sin(x + sinPos);
	}

    float splashWave(float x) {
		float result = 0f;
		if (sinPos > 10 & sinPos < 20) {
			result += 3*Mathf.Sin((sinPos/10-1)*(Mathf.PI)) * Mathf.Sin(x*Mathf.PI);
		}
		return result;
	}

    void UpdateMesh()
    {
		float firstx = vertices [0].x;
		float lastx = vertices[mesh.vertexCount - 1].x;
        for (var i = 0; i < mesh.vertexCount; i++)
        {
            var vert = vertices[i];
			vert.Set(vert.x, basicWave(vert.x) + splashWave((vert.x-firstx)/(lastx-firstx)), vert.z);
            vertices[i] = vert;
            sinPos += seaSpeed;
        }
    
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        transform.GetComponent<MeshCollider>().sharedMesh = mesh;

    }
}
