﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class AudioWaveShape : MonoBehaviour {
    AudioSource myAudio;
    public string[] inputDevices;
    private int deviceNum;
    public string CurrentAudioInput;
    public float maxRecordTime = 5.0f;

    public GameObject countPanel;
    GameObject[] sampleBoxesP1 = new GameObject[0], sampleBoxesP2 = new GameObject[0];
    public int samples = 10;
    public float sampleZoneSize = 15.5f;
    public GameObject sampleBoxP1, sampleBoxP2;
    public Transform playerPosition;
    public float sampleboxSpacing = 1.0f;
    public Slider recordSlider;
    public Transform wave1Parent, wave2Parent;
    int currentPlayer;

    public float sampleMultiplier = 1000.0f;
    bool recording;
    float recordStartTime;
    // Use this for initialization
    void Start () {
        myAudio = GetComponent<AudioSource>();
        inputDevices = new string[Microphone.devices.Length];
        deviceNum = 0; // Microphone.devices.Length - 1;

        for (int i = 0; i < Microphone.devices.Length; i++)
            inputDevices[i] = Microphone.devices[i].ToString();

        CurrentAudioInput = Microphone.devices[0].ToString();
        recording = false;
        currentPlayer = 1;
        sampleboxSpacing = sampleZoneSize / samples;
        Debug.Log("Start Recording Player 1");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("Fire1"))
        {
            countPanel.SetActive(true);
            Invoke("StartRecord", 3.5f);
            //StartRecord();
        }
        if (recording)
        {
            recordSlider.value = Time.time - recordStartTime;
            //Debug.Log(Time.time - recordStartTime);
            if (Time.time - recordStartTime > maxRecordTime)
            {
                countPanel.SetActive(false);
                StopRecord();
            }
        }
        if (!recording)
        {
            if (Input.GetKeyDown(KeyCode.P))
                myAudio.Play();
        }
	}

    void StartRecord()
    {
        recordStartTime = Time.time;
        Debug.Log("Start Recording");
        myAudio.clip = Microphone.Start(CurrentAudioInput, false, (int)maxRecordTime, samples);
        recording = true;
        //Clean the Waves
        int j = 0;
        if (currentPlayer == 1)
        {
            if (sampleBoxesP1.Length > 0)
            {
                while (j < sampleBoxesP1.Length)
                {
                    Destroy(sampleBoxesP1[j]);
                    j++;
                }
            }
        }
        if (currentPlayer == 2)
        {
            if (sampleBoxesP2.Length > 0)
            {
                while (j < sampleBoxesP2.Length)
                {
                    Destroy(sampleBoxesP2[j]);
                    j++;
                }
            }
        }
    }

    void StopRecord()
    {
        Debug.Log("Stop Recording");
        recording = false;
        Microphone.End(CurrentAudioInput);
        ProcessAudio();
        if (currentPlayer == 2)
            currentPlayer = 1;
        else
            currentPlayer = 2;
    }


    void ProcessAudio()
    {
        Debug.Log("Process Audio");
        if (currentPlayer == 1)
        {
            sampleBoxesP1 = new GameObject[samples];
            float[] audioSamples = new float[samples];
            myAudio.clip.GetData(audioSamples, 0);
            //Debug.Log(myAudio.clip.samples);
            int i = 0;
            while (i < audioSamples.Length)
            {
                float scaleY = audioSamples[i] * sampleMultiplier;
                //Debug.Log("sample: " + i + " = " + (audioSamples[i] * sampleMultiplier));
                Vector3 boxPosition = new Vector3(playerPosition.position.x + (i * sampleboxSpacing), playerPosition.position.y, playerPosition.position.z);
                sampleBoxesP1[i] = GameObject.Instantiate(sampleBoxP1, boxPosition, Quaternion.identity);
                sampleBoxesP1[i].transform.localScale = new Vector3(1, scaleY, 1);
                sampleBoxesP1[i].transform.parent = wave1Parent;
                i++;
            }
        }
        if (currentPlayer == 2)
        {
            sampleBoxesP2 = new GameObject[samples];
            float[] audioSamples = new float[samples];
            myAudio.clip.GetData(audioSamples, 0);
            //Debug.Log(myAudio.clip.samples);
            int i = 0;
            while (i < audioSamples.Length)
            {
                float scaleY = audioSamples[i] * sampleMultiplier;
                //Debug.Log("sample: " + i + " = " + (audioSamples[i] * sampleMultiplier));
                Vector3 boxPosition = new Vector3(playerPosition.position.x + (i * sampleboxSpacing), playerPosition.position.y, playerPosition.position.z);
                sampleBoxesP2[i] = GameObject.Instantiate(sampleBoxP2, boxPosition, Quaternion.identity);
                sampleBoxesP2[i].transform.localScale = new Vector3(1, scaleY, 1);
                sampleBoxesP2[i].transform.parent = wave2Parent;
                i++;
            }
        }
    }

    void PlayerChallenge()
    {

    }
}
