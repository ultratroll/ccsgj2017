﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModifyMesh : MonoBehaviour {

    public MeshFilter meshFilter;
    public Mesh mesh;
    private Vector3[] vertices;

    public float seaSpeed = 0.1f;
    public float seaHeight = 1f;

    private float sinPos = 0f;

    // Use this for initialization
    void Start () {
        mesh = meshFilter.mesh;
        vertices = mesh.vertices;
        Debug.Log("vertices: "+vertices.Length);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        UpdateMesh();
    }

    void UpdateMesh()
    {
        for (var i = 0; i < mesh.vertexCount; i++)
        {
            var vert = vertices[i];

            vert.Set(vert.x, seaHeight * Mathf.Sin(vert.x + sinPos), vert.z);
            vertices[i] = vert;
            sinPos += seaSpeed;
        }
    
        mesh.vertices = vertices;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        transform.GetComponent<MeshCollider>().sharedMesh = mesh;

    }
}
