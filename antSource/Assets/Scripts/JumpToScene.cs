﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpToScene : MonoBehaviour {

    public string SceneName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnJump()
    {
        Application.LoadLevel(SceneName);
    }
}
