﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharBehaviour : MonoBehaviour {


    //declaraciones

    //física y movimiento
    private Rigidbody rb;
    public float speed;
    public float PositionX;
    public float PositionZ;
    public float DistanceX;
    public float DistanceZ;
    public float Range;

    
    public float force;
    

    //stats y texts
    public int PlayerLife;
    public int Score;
    public Text ScoreText;
    public Text Life;

    //colision y dano
    public float TimerDmg;
    public Color collideColor;
    public Color normalColor;

    //salto de escena
    public string WinScene;
    public string SceneGameOver;

    //sound effects
    public AudioSource CandySFX;

    public GameObject vfxWave;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        PlayerLife = 3;
        Score = 0;
        ScoreText.text = "Score: " + Score.ToString();
        Life.text = "Remaining Lives: " + PlayerLife.ToString();
        TimerDmg = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (TimerDmg > 0) {
            TimerDmg -= Time.deltaTime;
        }

        //control de movimiento 

        if (Input.GetMouseButtonDown(0))
        {

            //vector para determinar posición mouse, devuelve número entre -0.5 y 0.5; donde 0,0 es posición character
            PositionX = Input.mousePosition.x / Screen.width - 0.5f;
            PositionZ = Input.mousePosition.y / Screen.height - 0.5f;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray))
            {
                //Instantiate(particle, transform.position, transform.rotation);
                Instantiate(vfxWave, transform.position, transform.rotation);
            }

            Vector2 v2mouse = new Vector2(PositionX, PositionZ);

            //vector para distancia relativa, dará un número entre 0,5 y 1

            DistanceX = 1 - Mathf.Abs(Input.mousePosition.x / Screen.width - 0.5f);
            DistanceZ = 1 - Mathf.Abs(Input.mousePosition.y / Screen.height - 0.5f);

            Vector2 v2distance = new Vector2(DistanceX, DistanceZ);

            //calculo rango de acción para limitar efectos de movimiento muy lejos de character
            Range = DistanceX + DistanceZ;

            //solo efecto por encima de threshold
            if (Range >= 1.52) {

                //limpieza de fuerzas residuales
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;

                force = Range * speed;
                Vector3 movement = new Vector3(-PositionX, 0.0f, -PositionZ);
                rb.AddForce(movement * force);
            }
            
        }
    }

//colisiones

void OnTriggerEnter(Collider other)
{
    if (other.gameObject.CompareTag("Rock"))
    {
        if(TimerDmg > 0) {
           return;
        }

        TimerDmg = 5f;
        PlayerLife--;
 

        if (PlayerLife == 0) {
            Application.LoadLevel(SceneGameOver);
        }

        Life.text = "Remaining Lives: " + PlayerLife.ToString();
            
        //flasheo daño 
        StartCoroutine(Flash());
            
    }

    Debug.Log("tag " + other.gameObject.tag);

        if (other.gameObject.CompareTag("Candy"))
        {

            other.gameObject.SetActive(false);
            Score = Score + 1000;
            ScoreText.text = "Score: " + Score.ToString();
            Instantiate (CandySFX, new Vector3 (transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            

        }

    if (Score == 3000)
    {
        Application.LoadLevel(WinScene);
    }


}

IEnumerator Flash()
{
    for (int i = 1; i <= 5; i++)
    {
        GetComponent<Renderer>().material.color = collideColor;
        yield return new WaitForSeconds(0.3f);
        GetComponent<Renderer>().material.color = normalColor;
        yield return null;
    }
}

}