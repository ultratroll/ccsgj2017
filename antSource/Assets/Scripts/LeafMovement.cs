﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LeafMovement : MonoBehaviour {
    public Text ScoreText;
    public Text Life;
    private Rigidbody rb;
    public float speed;
    private float distance;
    public float posX;
    public float posZ;
    public float ValorDist;
    public float force;
    public float maxForce;
    public float mouseX;
    public float leafX;
    public int PlayerLife;
    public int Rebote;
    public int Score;
    public string SceneGameOver;

    public Color collideColor;
    public Color normalColor;
    public string WinScene;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        PlayerLife = 3;
        Score = 0;
        ScoreText.text = "Score: " + Score.ToString();
        Life.text = "Remaininc Lives: " + PlayerLife.ToString();
    }
	
	// Update is called once per frame
	void Update () {
        //control de movimiento 
        if (Input.GetMouseButtonDown(0)) {

            Vector2 v2Leaf  = new Vector2 (transform.position.x, transform.position.z);
            Vector2 v2mouse = new Vector2 (Input.mousePosition.x-Screen.width/2, Input.mousePosition.y - Screen.height/2);
        
            var distance = Mathf.Abs (Vector2.Distance(v2mouse, v2Leaf));


            ValorDist = distance;

            float MovDif = (160 - distance)/160;
             if (MovDif < 0) {
                    MovDif = 0;
             }

            force = MovDif;
            
            if (MovDif >= 0) {
                float SenX = ((Input.mousePosition.x - Screen.width / 2) - transform.position.x)/distance;          
                float SenZ = ((Input.mousePosition.y - Screen.height / 2) - transform.position.z) /distance;

                mouseX = Input.mousePosition.x - Screen.width / 2;
                leafX = transform.position.x;

                posX = SenX;
                posZ = SenZ;

                Vector3 movement = new Vector3(-SenX, 0.0f, -SenZ);

                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;

                force =  MovDif * speed;

                if (force > maxForce) force = maxForce;

                rb.AddForce(movement * force);
            }
        }
    }

    //colisiones

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Rock")) {
            PlayerLife--;
            
            if (PlayerLife== 0) {
                Application.LoadLevel(SceneGameOver);
            }

            Life.text = "Remaininc Lives: " + PlayerLife.ToString();
            
            //flasheo daño y rebote
            StartCoroutine(Flash());

            //rb.AddRelativeForce(Vector3.back * Rebote);
            //rb.AddRelativeForce(Vector3.left * Rebote);
        }

        Debug.Log("tag " + other.gameObject.tag);

        if (other.gameObject.CompareTag("Candy")) {
            Debug.Log("yei");
            other.gameObject.SetActive(false);
            Score = Score + 1000;
            ScoreText.text = "Score: " + Score.ToString();
        }

        if (Score == 3000) {
            Application.LoadLevel(WinScene);
        }


    }

    IEnumerator Flash() {
        for (int i = 1; i <= 5; i++) {
            GetComponent<Renderer>().material.color = collideColor;
            yield return new WaitForSeconds(0.3f);
            GetComponent<Renderer>().material.color = normalColor;
            yield return null;   
        }
    }
}