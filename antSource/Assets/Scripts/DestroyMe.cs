﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyMe : MonoBehaviour {

    float time = 2f;

	// Use this for initialization
	void Start () {
        Invoke("Remove", time);
	}
	
	// Update is called once per frame
	void Remove () {
        Destroy(this.gameObject);
	}
}
