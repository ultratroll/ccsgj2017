﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {


    private Rigidbody rb;
    public float EnemySpeed;
    public float TimerMov;
    public float ReactionTime;


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        TimerMov = ReactionTime;
    }
	
	// Update is called once per frame
	void Update () {
        TimerMov += Time.deltaTime;
        
        if (TimerMov > ReactionTime) {
            Vector3 EnemyMov = new Vector3(2*Random.value-1, 0.0f, 2*Random.value-1);
            rb.AddForce(EnemyMov * EnemySpeed);
            TimerMov = 0;
        }


    }
}
