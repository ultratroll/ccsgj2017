﻿using UnityEngine;
using System.Collections;

public class twerk : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
    
	// Update is called once per frame
	void Update () {

        do {
            transform.Rotate(Vector3.forward * Time.deltaTime * speed);
        }
        while (transform.rotation.z > -10.5);


        do
        {
            transform.Rotate(Vector3.back * Time.deltaTime * speed);
        }
        while (transform.rotation.z < 3.5);

    }
}
